# Web Socket Example

## Purpose

This is an example project about websockets based upon a book club I lead at work. We are finishing up reading [High Performance Browser Networking](https://hpbn.co/) by [Ilya Grigorik](https://www.igvita.com/).
